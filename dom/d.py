#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import time
import couchdb
import pwiDB

from daemon import runner

s = couchdb.Server('http://194.29.175.241:5984')
db = s['p5']


class App:
    def __init__(self):
        self.stdin_path = '/dev/null'
        self.stdout_path = '/dev/tty'
        self.stderr_path = '/dev/tty'
        self.pidfile_path = '/tmp/demon_p5_3.pid'
        self.pidfile_timeout = 5

    def run(self):
        global db
        i = 0
        while True:
            try:
                logger.info('Rozpoczecie aktualizacji bazy')
                logger.info('PO RAZ '+ str(i))

                result = pwiDB.parse('/var/log/nginx/access.log', logger)
                logger.info((str(len(result))))
                pwiDB.add(result,logger)

            except Exception, e:
                logger.info('Pojawily sie bledy. Baza mogla nie zostac zaktualizowana')

            finally:
                i += 1
                logger.info('Zakonczono aktualizacje. Następna aktualizacja za 180 sekund.')
                time.sleep(180)


logger = logging.getLogger("DemonLog")
logger.setLevel(logging.INFO)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
handler = logging.FileHandler("/home/p5/demon_p5_3.log")
handler.setFormatter(formatter)
logger.addHandler(handler)

app = App()
daemon_runner = runner.DaemonRunner(app)

daemon_runner.daemon_context.files_preserve = [handler.stream]
daemon_runner.do_action()