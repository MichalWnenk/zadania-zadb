#!/usr/bin/env python
# -*- coding: utf-8 -*-
import apachelog, couchdb

s = couchdb.Server('http://194.29.175.241:5984')
db = s['p5']

items = 0

def parse(file, logger):
    logger.info('Parsowanie pliku: '+file)

    log_format = r'%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"'
    parser = apachelog.parser(log_format)

    output = []
    file = open(file)

    for line in file:
        try:
            output.append(parser.parse(line))
        except:
            logger.error("Problem z parsowaniem %s" % line)

    logger.info('Zakonczono parsowanie')
    return output

def add(log, logger):
    global items

    logger.info('Rozpoczeto sprawdzanie nowych informacji o obiektach w logu')
    logger.info('Liczba obiektów w logu: '+ str(items))

    if len(log) <= items:
        logger.info('Brak nowych elementow w logu')
        return

    logger.info('Rozpoczecie dodawania nowych wpisow do bazy')
    for info in log[items-1:]:
        ip = info['%h']
        size = info['%b']
        date = info['%t']
        try:
            items += 1
            logger.info('Wywolanie metody db.save')
            db.save({'ip': ip, 'length': size, 'datetime': date})
            logger.info('DODANO informacje o adresie ip: '+ ip)
        except:
            logger.error('Nastapil blad z zapisem danych do bazy. Dane dla obiektu "IP: %s; size: %s; datetime: %s" nie zostaly zapisane', ip, size, date)
            pass

    logger.info('Liczba nowych dokumentow w bazie: '+ items)
    logger.info('Zakonczono dodawanie')

