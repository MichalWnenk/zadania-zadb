# -*- coding: utf-8 -*-

import json
import unittest

from rsmr import MapReduce


def map(item):
    key = item[1]
    value = [item[0]] + item[2:]
    yield key, value


def reduce(key, values):
    for x in values:
        if x[0] == 'order':
            for y in values:
                if y[0] == 'line_item':
                    yield [key] + x + y


def read_data(filename):
    data = open(filename)
    return json.load(data)


def join_records():
    input_data = read_data('records.json')
    mapper = MapReduce(map, reduce)
    results = mapper(input_data)
    return results


class JoinTest(unittest.TestCase):

    def test_results(self):
        expected = read_data('join.json')
        current = join_records()
        self.assertListEqual(sorted(expected),sorted(current))


if __name__ == '__main__':
    unittest.main()
